@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import kotlin.math.roundToInt
import kotlin.system.exitProcess

if (args.size != 1) {
    println("usage: emitlogtopic <topic>")
    exitProcess(1)
}

val EXCHANGE_NAME = "topic_logs"
val factory = ConnectionFactory()
factory.host = "localhost"
factory.newConnection().use { connection ->
    val channel = connection.createChannel()
    channel.exchangeDeclare(EXCHANGE_NAME, "topic")
    val message = "[${args[0]}] test"
    channel.basicPublish(EXCHANGE_NAME, args[0], null, message.toByteArray())
    println(" [x] Sent '$message'")
}
