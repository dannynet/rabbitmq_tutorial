@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.system.exitProcess

if (args.isEmpty()) {
    println("usage: receivelogstopic <topic-pattern>")
    exitProcess(1)
}

val EXCHANGE_NAME = "topic_logs"
val factory = ConnectionFactory()
factory.host = "localhost"
val connection = factory.newConnection()
val channel = connection.createChannel()

channel.exchangeDeclare(EXCHANGE_NAME, "topic")
val queueName = channel.queueDeclare().queue
args.forEach { topicPattern ->
    channel.queueBind(queueName, EXCHANGE_NAME, topicPattern)
}

log("Waiting for messages for ${args.joinToString("/")}. To exit press CTRL+C")

channel.basicConsume(
    queueName,
    true,
    { _, delivery ->
        val message = String(delivery.body)
        log("Received $message for ${delivery.envelope.routingKey}")
    },
    { _ ->  }
)

fun log(s: String) {
    val time = LocalDateTime.now().format(DateTimeFormatter.ISO_TIME)
    println("$time - $s")
}
