@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import kotlin.math.roundToInt
import kotlin.system.exitProcess

if (args.size != 2 || !args.all { it.matches("[0-9]+".toRegex()) }) {
    println("usage: newtask <number-of-tasks> <worker-duration-in-seconds>")
    exitProcess(1)
}

val EXCHANGE_NAME = "ch2a"
val factory = ConnectionFactory()
factory.host = "localhost"
factory.newConnection().use { connection ->
    val channel = connection.createChannel()
    channel.exchangeDeclare(EXCHANGE_NAME, "direct")
    (1..args[0].toInt()).forEach {
        val t = tenant()
        val message = "test-$t-$it-${".".repeat(duration())}"
        channel.basicPublish(EXCHANGE_NAME, t, null, message.toByteArray())
        println(" [x] Sent '$message'")
    }
}

fun duration() =
    (Math.random() * args[1].toInt()).roundToInt() + 1

fun tenant() =
    listOf("ne0001", "ne0002", "ne0003")[(Math.random() * 3).toInt()]
