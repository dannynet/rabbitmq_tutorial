@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.system.exitProcess

if (args.size != 1) {
    println("usage: worker <tenantcode>")
    exitProcess(1)
}

val EXCHANGE_NAME = "ch2a"
val factory = ConnectionFactory()
factory.host = "localhost"
val connection = factory.newConnection()
val channel = connection.createChannel()

channel.exchangeDeclare(EXCHANGE_NAME, "direct")
val queueName = "ch2a.tenant.${args[0]}"
channel.queueDeclare(queueName, false, false, false, null)
channel.queueBind(queueName, EXCHANGE_NAME, args[0])

channel.basicQos(1)
log("Waiting for messages on $queueName for ${args[0]}. To exit press CTRL+C")
val autoAck = false

channel.basicConsume(
    queueName,
    autoAck,
    { _, delivery ->
        val message = String(delivery.body)
        log("Received $message")
        try {
            doWork(message)
        } finally {
            log("Done")
            channel.basicAck(delivery.envelope.deliveryTag, false)
        }
    },
    { _ ->  }
)

fun doWork(task: String) {
    val duration = task.filter { it == '.' }.length
    Thread.sleep(duration * 1000L)
}

fun log(s: String) {
    val time = LocalDateTime.now().format(DateTimeFormatter.ISO_TIME)
    println("$time - $s")
}
