@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import kotlin.math.roundToInt
import kotlin.system.exitProcess

if (args.size != 2 || !args.all { it.matches("[0-9]+".toRegex()) }) {
    println("usage: newtask <number-of-tasks> <worker-duration-in-seconds>")
    exitProcess(1)
}

val QUEUE_NAME = "hello"
val factory = ConnectionFactory()
factory.host = "localhost"
factory.newConnection().use { connection ->
    val channel = connection.createChannel()
    channel.queueDeclare(QUEUE_NAME, false, false, false, null)
    (1..args[0].toInt()).forEach {
        val message = "test-$it-${".".repeat(duration())}"
        channel.basicPublish("", QUEUE_NAME, null, message.toByteArray())
        println(" [x] Sent '$message'")
    }
}

fun duration() =
    (Math.random() * args[1].toInt()).roundToInt() + 1
