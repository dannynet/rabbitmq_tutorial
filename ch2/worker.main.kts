@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val QUEUE_NAME = "hello"
val factory = ConnectionFactory()
factory.host = "localhost"
val connection = factory.newConnection()
val channel = connection.createChannel()

channel.queueDeclare(QUEUE_NAME, false, false, false, null)
channel.basicQos(1)
log("Waiting for messages. To exit press CTRL+C")
val autoAck = false

channel.basicConsume(
    QUEUE_NAME,
    autoAck,
    { consumerTag, delivery ->
        val message = String(delivery.body)
        log("Received $message for $consumerTag")
        try {
            doWork(message)
        } finally {
            log("Done")
            channel.basicAck(delivery.envelope.deliveryTag, false)
        }
    },
    { _ ->  }
)

fun doWork(task: String) {
    val duration = task.filter { it == '.' }.length
    Thread.sleep(duration * 1000L)
}

fun log(s: String) {
    val time = LocalDateTime.now().format(DateTimeFormatter.ISO_TIME)
    println("$time - $s")
}
