@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel

val QUEUE_NAME = "hello"
val factory = ConnectionFactory()
factory.host = "localhost"
factory.newConnection().use { connection ->
    val channel = connection.createChannel()
    channel.queueDeclare(QUEUE_NAME, false, false, false, null)
    val message = "Hello world!"
    channel.basicPublish("", QUEUE_NAME, null, message.toByteArray())
    println(" [x] Sent '$message'")
}
