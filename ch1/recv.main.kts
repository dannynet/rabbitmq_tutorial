@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback

val QUEUE_NAME = "hello"
val factory = ConnectionFactory()
factory.host = "localhost"
val connection = factory.newConnection()
val channel = connection.createChannel()

channel.queueDeclare(QUEUE_NAME, false, false, false, null)
println(" [*] Waiting for messages. To exit press CTRL+C")

channel.basicConsume(
    QUEUE_NAME,
    true,
    { consumerTag, delivery ->
        val message = String(delivery.body)
        println(" [x] Received $message")
    },
    { consumerTag -> println(" [*] Stop listening") }
)
