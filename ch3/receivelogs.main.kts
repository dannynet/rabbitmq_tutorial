@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val EXCHANGE_NAME = "logs"
val factory = ConnectionFactory()
factory.host = "localhost"
val connection = factory.newConnection()
val channel = connection.createChannel()

channel.exchangeDeclare(EXCHANGE_NAME, "fanout")
val queueName = channel.queueDeclare().queue
channel.queueBind(queueName, EXCHANGE_NAME, "")

log("Waiting for messages. To exit press CTRL+C")

channel.basicConsume(
    queueName,
    true,
    { consumerTag, delivery ->
        val message = String(delivery.body)
        log("Received $message for $consumerTag")
    },
    { _ ->  }
)

fun log(s: String) {
    val time = LocalDateTime.now().format(DateTimeFormatter.ISO_TIME)
    println("$time - $s")
}
