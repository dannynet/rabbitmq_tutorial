@file:DependsOn("com.rabbitmq:amqp-client:5.10.0")
@file:DependsOn("org.slf4j:slf4j-simple:1.7.30")

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import kotlin.math.roundToInt
import kotlin.system.exitProcess

if (args.size != 1 || !args.all { it.matches("[0-9]+".toRegex()) }) {
    println("usage: emitlog <number-of-tasks>")
    exitProcess(1)
}

val EXCHANGE_NAME = "logs"
val factory = ConnectionFactory()
factory.host = "localhost"
factory.newConnection().use { connection ->
    val channel = connection.createChannel()
    channel.exchangeDeclare(EXCHANGE_NAME, "fanout")
    (1..args[0].toInt()).forEach {
        val message = "test-$it"
        channel.basicPublish(EXCHANGE_NAME, "", null, message.toByteArray())
        println(" [x] Sent '$message'")
    }
}
